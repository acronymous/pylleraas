#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 10:28:49 2021

@author: william
"""

import argparse as ap
import json
import sys

import numpy as np
from matplotlib import pyplot as plt

from tools import algebra, pylleraas_util

display_registry = {}
class AutoRegDisplay(type):
    def __new__(mcls, name, bases, attrs, function_key=None):
        cls = type.__new__(mcls, name, bases, attrs)
        if function_key is not None:
            display_registry[function_key] = cls
        return cls


class DataDisplay(metaclass=AutoRegDisplay):
    displays = {}
    
    def __init__(self, data, name, func, basis):
        self.data = data
        self.name = name
        self.func = func
        self.basis = basis
        
    def render(self, axis, xs, **kwargs):
        pass
    

class EigenDisplay(DataDisplay, function_key="eigenvalue"):
    def render(self, axis, xs, min_vline=False, min_hline=False, **kwargs):
        # What we need to do:
        energies = np.fromiter((self.func([x], self.basis) for x in xs), 
                               dtype=np.float64)
        axis.plot(xs, energies, label=self.name)
        if min_hline:
            axis.axhline(self.data["minimize_result"]["fun"], 
                         label=self.name+": E minimum", linestyle="--")
        if min_vline:
            axis.axvline(self.data["minimize_result"]["x"][0], 
                         label=self.name+": minimal λ", linestyle="--")


class RawDisplay(DataDisplay, function_key="raw"):
    def render(self, axis, xs, min_vline=True, min_hline=True, **kwargs):
        optimum_lincomb = self.data["minimize_result"]["x"][1:]
        energies = np.fromiter((self.func([x]+optimum_lincomb, self.basis) 
                                for x in xs), 
                               dtype=np.float64)
        
        axis.plot(xs, energies, label=self.name)


argparser = ap.ArgumentParser()
argparser.add_argument("xrange", nargs=2, type=float)
argparser.add_argument("--data", "-d", nargs=2, action="append", required=True)
# ^ 1st argument should be a "short name", the second should be a path.
argparser.add_argument("--render-kwargs", "-rk", nargs=3, action="append",
                       dest="renderkwargs", default=[])
# ^ 1st argument is the "short name" of the data file, 2nd is the
#   key of that file's data set...
argparser.add_argument("--style", "-s", action="append")
argparser.add_argument("--xlim", "-xl", nargs=2, type=float)
argparser.add_argument("--ylim", "-yl", nargs=2, type=float)
argparser.add_argument("--vline", "-vl", action="append", type=float)
argparser.add_argument("--hline", "-hl", action="append", type=float)

args = argparser.parse_args()

# TODO: Implement integration with matplotlib's stylesheets.


# Loading in data from json files...
render_data = {}
basis_data = {}
for name, dat in args.data:
    try:
        dat_file = open(dat, "r")
    except OSError as err:
        print(err, file=sys.stderr)
        continue
    with dat_file:
        try:
            dat_json = json.load(dat_file)
            render_data[name] = dat_json["data"]
            basis_data[name] = dat_json["basis"]
        except json.JSONDecodeError as err:
            print(err, file=sys.stderr)
            continue
        
# Process renderkwargs for each data set to plot...
# These dictionaries will be passed into each call of <subclass of DataDisplay>.render()
render_kwargs = {k1:{k2:{} for k2 in v.keys()} 
                 for k1, v in render_data.items()}
for data_name, set_name, kw_json in args.renderkwargs:
    try:
        # Putting all these here so that common errors (from typos in CLI)
        # will fail gracefully instead of crashing the program.
        dict_to_update = render_kwargs[data_name][set_name]
        kw_json_decoded = json.loads(kw_json)
        assert type(kw_json_decoded) == dict  # Input validation
    except (KeyError, json.JSONDecodeError, AssertionError) as err:
        print(err, file=sys.stderr)
        continue
    dict_to_update.update(kw_json_decoded) 
    # Updating instead of assigning so that later arguments can override
    # earlier ones selectively.
    
# Now, let's render the plot!
fig = plt.figure()
ax = fig.add_subplot(111)

xs = np.linspace(*args.xrange, 500)
# We need to iterate over each data set contained in render_data.
for short_name, datasets in render_data.items():
    # Getting our basis together...
    basis = [tuple(b) for b in basis_data[short_name]]
    basis_states = [algebra.HyllerasState(basis, arr)
                    for arr in np.identity(len(basis))]
    # Inner loop over the various functions optimised using this basis.
    for func_used, data in datasets.items():
        kw = render_kwargs.get(short_name, {}).get(func_used, {})
        
        # Getting the right thing to draw with, and the right function: 
        # a subclass of DataDisplay and a function from
        # pylleraas_util.func_choices_dict.
        try:
            renderer_class = display_registry[func_used]
            func = pylleraas_util.func_choices_dict[func_used]
        except KeyError as err:
            # Fail gracefully if we can't find either of those
            print(err, file=sys.stderr)
            continue
        
        name = short_name+"/"+func_used
        # \/ Actually call the requisite drawing code.
        renderer_class(data, name, func, basis_states).render(ax, xs, **kw)

# Vertical and horizontal lines
if args.vline is not None:
    for vline in args.vline:
        ax.axvline(vline)
if args.hline is not None:
    for hline in args.hline:
        ax.axhline(hline,
                   label="Ground state reference", c="k") 
        # HACK: need a way of labelling/styling vertical lines.

if args.xlim is not None:
    ax.set_xlim(args.xlim)
if args.ylim is not None:
    ax.set_ylim(args.ylim)

ax.set_xlabel("λ")
ax.set_ylabel("E / Eₕ")
ax.legend()
plt.show()