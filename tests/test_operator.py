#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test module for algebra.Operator, verifying the following:
    * That the sandwiching functions (util.T, util.C_hat, etc.) the operators wrap 
      are commutative, by swapping the js, ks, and ms fed into them.
    * 

Created on Fri Oct 15 14:44:16 2021

@author: william
"""
import itertools
from sys import maxsize as BIGNUMBER
import unittest as ut

import numpy as np

from tools import algebra
from tools import util
from . import testdata


class OperatorTests(ut.TestCase):
    
    def validate_sandwich(self, operator: algebra.Operator, 
                          left_state: algebra.HyllerasState, 
                          right_state: algebra.HyllerasState, 
                          validator, *args, name="", test_msg=None, **kwargs):
        """Utility function for testing if a sandwich is producing the right result."""
        with self.subTest(sort="Operator sandwich validation", name=name):
            result = operator.sandwich(left_state, right_state, *args, **kwargs)
            should_be = validator(left_state, right_state, *args, **kwargs)
            should_be_this = f"\n\tResult ({result}) should be about {should_be}\n\tError: {should_be-result} Factor: {should_be/result}" 
            if test_msg is not None:
                out_msg = test_msg+": "+should_be_this
            else:
                out_msg = should_be_this
            self.assertTrue(np.isclose(result, should_be),
                             msg=out_msg)

    def test_single_validation(self):
        """Tests that operator sandwiches are producing the right values."""
        ops = [(algebra.T, algebra.T_validator),
               (algebra.C_hat, algebra.C_validator),
               (algebra.W, algebra.W_validator),
               (algebra.I, algebra.I_validator)]
        iteration = itertools.product(ops, testdata.h_states_product(2))
        for (op, validator), (left, right) in iteration:
            self.validate_sandwich(op, left, right, validator, util.L_k_swap(2),
                                   name=f"<{left}|{op}|{right}>")

    def test_swap_invariance(self):
        """Tries to demonstrate commutativity in J, K, and M for each 
           sandwiching function."""
        funcs_to_test = [util.N, util.C_hat, util.W, util.T]
        L = 71
        Js = [2, 4]
        Ks = [1, 8]
        Ms = [1, 4]
        
        func_args = ((j1, j2, k1, k2, m1, m2, L)
                     for (j1, j2), (k1, k2), (m1, m2) in itertools.product(
                             itertools.permutations(Js),
                             itertools.permutations(Ks),
                             itertools.permutations(Ms)))
        # ^ Creating a generator which will produce every combination of 
        #  arguments with the Js, Ks, Ms swapped or not swapped. Helps make
        #  the test a bit more thorough.
        func_args_tee = itertools.tee(func_args, len(funcs_to_test))
        # ^ Duplicating the generator: keeping it out of the func_results
        #   expression means it isn't recreated on each iteration through func_results,
        #   so we need spares.
        
        func_results = (itertools.starmap(func, arg_iter) 
                        for func, arg_iter in zip(funcs_to_test, func_args_tee))
        # And this creates our result generators! Liberal use of generators
        # like this means that the memory footprint won't grow much at all
        # if this test needs to grow in future.
        
        for result_group in func_results:
            group_a, group_b = itertools.tee(result_group)
            # ^ Duplicating the generator of results from each function
            results_next = itertools.islice(group_b, 1, BIGNUMBER)
            # ^ Starting from the second result of one of the generators...
            for n, (result, next_result) in enumerate(zip(group_a, results_next)):
                # Exploiting the transitivity of equality: we only need to
                # compare the first value to the next value.
                with self.subTest(n):
                    self.assertEqual(result, next_result)
                    
    def test_summation(self):
        """Tests if summation of operator values is working as expected."""
        ops = [(algebra.T, algebra.T_validator),
               (algebra.C_hat, algebra.C_validator),
               (algebra.W, algebra.W_validator),
               (algebra.I, algebra.I_validator)]
        iteration = itertools.product(ops, ops, testdata.h_states_product(2))
        for (op1, val1), (op2, val2), (left, right) in iteration:
            test_op = op1 + op2
            def validator(left, right, *args, **kwargs):
                return (val1(left, right, *args, **kwargs)
                        + val2(left, right, *args, **kwargs))
            self.validate_sandwich(test_op, left, right, validator, util.L_k_swap(2),
                                   name=f"<{left}|{test_op}|{right}>")
        