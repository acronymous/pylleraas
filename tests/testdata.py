#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains a bunch of data used by the unit tests, that was making
the test modules too cumbersome. Mostly lists of well-formed HyllerasStates
and tools for iterating over them.

Created on Thu Oct 14 21:35:04 2021

@author: william
"""
import itertools

import numpy as np

from tools import algebra
from tools import util

#
# Well-formed basis states
#
def good_states_product(n):
    return itertools.product(*([good_states]*n))

good_states = [algebra.HyllerasState(vectors=[(0, 0, 0), 
                                              (1, 1, 0), 
                                              (3, 4, 5), 
                                              (12, 4, 4)],
                                     array=np.array([1, 0, 0, 0])),
               algebra.HyllerasState(vectors=[(0, 0, 0), 
                                              (1, 1, 0), 
                                              (3, 4, 5), 
                                              (12, 4, 4)],
                                     array=np.array([0, 1, 0, 0])),
               algebra.HyllerasState(vectors=[(0, 0, 0), 
                                              (1, 1, 0), 
                                              (3, 4, 5), 
                                              (12, 4, 4)],
                                     array=np.array([1, 1, 0, 0])),
               algebra.HyllerasState(vectors=[(0, 0, 0), 
                                              (5, 6, 7),
                                              (1, 1, 0),
                                              (12, 4, 4)],
                                     array=np.array([1, 1, 0, 1])),
               algebra.HyllerasState(vectors=[(0, 0, 0), 
                                              (1, 1, 0), 
                                              (3, 4, 5), 
                                             (12, 4, 4)],
                                     array=np.array([0, 0, 0, 0])),
               algebra.HyllerasState(vectors=[(0, 0, 0), 
                                              (5, 6, 7),
                                              (1, 1, 0),
                                              (12, 4, 4)],
                                     array=np.array([0, 0, 0, 0])),
               ]


def h_states_product(n):
    return itertools.product(*([h_states]*n))

#
# The basis that Hylleraas and Undheim used, which we have a pre-computed
# "known-good" function for.
#
h_states = [algebra.HyllerasState(vectors=[(0, 0, 0), 
                                          (0, 0, 1), 
                                          (0, 2, 0)],
                                 array=np.array([1, 0, 0]),
                                 name="H000"),
           algebra.HyllerasState(vectors=[(0, 0, 0), 
                                          (0, 0, 1), 
                                          (0, 2, 0)],
                                 array=np.array([0, 1, 0]),
                                 name="H001"),
           algebra.HyllerasState(vectors=[(0, 0, 0), 
                                          (0, 0, 1), 
                                          (0, 2, 0)],
                                 array=np.array([0, 0, 1]),
                                 name="H020"),
           ]