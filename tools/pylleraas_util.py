#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 23:37:01 2021

@author: william

High-level utility functions for 
"""

import functools
import itertools
import json
import operator as op
import sys

import numpy as np

from . import algebra

#
# So, the plan is to minimise the eigenvalues of a certain matrix.
# This matrix, P, is given by P=U*Y.T*H*Y*U, where:
#   * U is the diagonal inverse square root of N, the inner product matrix
#     of the basis states.
#   * Y is the matrix of eigenvectors of N.
#   * H is the Hamiltonian matrix

#
# First off, we need a way of generating this matrix.
#

def P(states, L):
    N_results = np.fromiter((algebra.I.sandwich(s1, s2, L)
                             for s1, s2 in itertools.product(states, states)),
                            dtype=np.float64)
    N_mat = N_results.reshape((len(states), len(states)))
    # ^ If anyone knows a better way of generating a square matrix from an
    #   iterable, let me know.
    
    evs, Y = np.linalg.eig(N_mat) # Eigenvalues and eigenvectors
    
    U = np.diag(np.fromiter((1/np.sqrt(eigenvalue) for eigenvalue in evs),
                            dtype=np.float64))
    
    H_results = np.fromiter((algebra.H.sandwich(s1, s2, L) 
                             for s1, s2 in itertools.product(states, states)),
                            dtype=np.float64)
    H = H_results.reshape((len(states), len(states)))
    return U @ Y.T @ H @ Y @ U

def extra_default(**kwargs):
    return {}

#
# Now, we need a function to minimise. Makes sense to minimise the minimum
# eigenvalue...
# 

def eigen_func(params, states):
    energies = np.linalg.eigvals(P(states, params[0]))
    return np.min(energies)
eigen_func.param_length = 1

def eigen_extra(minimize_result=None, basis_states=[], **kwargs):
    if minimize_result is None:
        raise RuntimeError("No optimisation result provided.")
    else:
        energies, eigenvectors = np.linalg.eig(P(basis_states, 
                                                 minimize_result.x[0]))
        return {"eigenenergies": energies,
                "eigenvectors": eigenvectors}

eigen_func.extra_func = eigen_extra  
# ^ So that we can output the data returned by it 

# "Raw" function to compare performance against.
def full_opt_func(params, states):
    state = functools.reduce(op.add, (c*state for c, state in zip(params[1:], states)))
    return algebra.H.sandwich(state, state, params[0])/algebra.I.sandwich(state, state, params[0])
# param_length for this function is set at runtime.


func_choices_dict = {"eigenvalue":eigen_func,
                     "raw":full_opt_func}
   

#
# JSON serialisation utilities.
#
def simple_serialiser(obj):
    if type(obj) == np.ndarray:
        return obj.tolist()
    else:
        raise TypeError


def json_dump_to_files(filenames, obj, /, mode="x"):
    for output_file in filenames:
        try:
            outfile = open(output_file, "w")
        except OSError as err:
            print(f"Failed to write to {output_file}: {err}",
                  file=sys.stderr)
            continue
        with outfile:
            json.dump(obj, outfile, indent="\t",
                      default=simple_serialiser)