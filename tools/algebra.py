#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains the bulk of the code related to working with the Hilbert
space of Hylleraas' helium basis states.

Includes HyllerasState, a class representing an element of this Hilbert space 
with a finite number of select basis states, Operator, a class for 

Created on Wed Oct 13 17:06:21 2021

@author: william
"""
import itertools
import functools

import numpy as np

from . import util
from . import testutils


class HyllerasState():
    """
    A class for representing a quantum state as used in Hylleras' variational
    calculation for the energy levels of helium.
    """
    def __init__(self, vectors=[], array=np.array([]), name="_"):
        # Deduplicate vectors. This attribute is used to keep track of the
        #                      specific basis vectors tracked by self.array.
        self._vecs = list(dict.fromkeys(vectors))

        # This tracks the exact linear combination of the vectors specified by
        # self._vecs which forms this state.
        # Maybe we should check whether the array is the right shape and dtype?
        self.array = array
        
        self.name = name  # Just a handy way of keeping track...

    @classmethod
    def from_dict(cls, vectors=[], dictionary={}, **kwargs):
        """
        Constucts a new HyllerasState from a list of j-k-m tuples and a
        dictionary describing their linear combination.
        """
        # Deduplicate vectors. This attribute will be used to keep track of the
        #                      "basis signature" of the array.
        vecs = list(dict.fromkeys(vectors))
        arr = np.array([dictionary.get(vec, 0) for vec in vecs],
                       dtype=np.complex256)
        return cls(vectors=vecs, array=arr, **kwargs)

    #
    # Ensuring that the states behave like a vector space.
    #
    def __add__(self, rhs):
        if not isinstance(rhs, HyllerasState):
            raise TypeError(f"unsupported operand type(s) for +: 'HyllerasState' and '{type(rhs)}'")

        if self._vecs == rhs._vecs:
            # Element-wise equality implies that we can just add the arrays
            # together. No combination/deduplication needed!
            return HyllerasState(self._vecs, self.array + rhs.array)
        else:
            # Handles the case where two HyllerasStates have mismatched signatures.
            # Prioritises the signature of the LHS.
            # Probably faster than sorting the signature after every new basis
            # vector is added...
            new_vectors = list({**dict.fromkeys(self._vecs),
                                **dict.fromkeys(rhs._vecs)})

            new_combs = {}
            for vec in new_vectors:
                try:
                    idx_l = self._vecs.index(vec)
                    val_l = self.array[idx_l]
                except ValueError:
                    val_l = 0

                try:
                    idx_r = rhs._vecs.index(vec)
                    val_r = rhs.array[idx_r]
                except ValueError:
                    val_r = 0

                new_combs[vec] = val_l + val_r

            return HyllerasState.from_dict(vectors=new_vectors,
                                           dictionary=new_combs,
                                           name=f"{self.name}+{rhs.name}")

    def __mul__(self, rhs):
        return HyllerasState(self._vecs, rhs*self.array, 
                             name=f"{rhs}*({self.name})")

    def __rmul__(self, lhs):
        return self.__mul__(lhs)
    
    def __str__(self):
        # self.name is a useful string representation this specific vector.
        return self.name


class Operator():
    """
    Class for representing the action of a linear operator on a HyllerasState.

    Currently, only sandwiching the operator between two HyllerasStates is
    described.
    """
    def __init__(self, sandwich=None, name="_"):
        assert sandwich is not None
        self.sandwich = sandwich
        self.name = name
        
    #
    # Defining addition and multiplicaton operations on Operators, so that they 
    # behave like a vector space too.
    #
    def __add__(self, rhs):
        if not isinstance(rhs, Operator):
            raise TypeError(f"unsupported operand type(s) for +: 'Operator' and '{type(rhs)}'")

        def new_sandwich(left_state, right_state, *args, **kwargs):
            return (self.sandwich(left_state, right_state, *args, **kwargs)
                    + rhs.sandwich(left_state, right_state, *args, **kwargs))

        return Operator(new_sandwich, name=f"{self.name}+{rhs.name}")

    def __mul__(self, rhs):
        if not isinstance(rhs, complex):
            raise TypeError(f"unsupported operand type(s) for *: 'Operator' and '{type(rhs)}'")

        def new_sandwich(left_state, right_state, *args, **kwargs):
            return rhs*self.sandwich(left_state, right_state, *args, **kwargs)

        return Operator(sandwich=new_sandwich, name=f"{rhs}*({self.name})")
    
    def __rmul__(self, lhs):
        return self.__mul__(lhs)
    
    def __str__(self):
        return self.name


def linear_sandwich(f):
    """
    Decorator for turning a function that operates on (j1, j2, k1, k1, m1, m2,
    etc...) into a bilinear function on HyllerasStates.
    """
    @functools.wraps(f)
    def linear_f(left_state, right_state, *args, **kwargs):
        products = itertools.product(enumerate(left_state._vecs),
                                     enumerate(right_state._vecs))

        return sum(
                (  left_state.array[l_index].conjugate()
                 * right_state.array[r_index]
                 * f(j1, j2, k1, k2, m1, m2, *args, **kwargs))
                for (l_index, (j1, k1, m1)), (r_index, (j2, k2, m2)) in products
               )
    return linear_f

#
# Defining a few functions based on the above classes.
#

# Making our kinetic, coulomb, and inter-electron operators.
T = Operator(sandwich=linear_sandwich(util.T), name="T")
C_hat = Operator(sandwich=linear_sandwich(util.C_hat), name="C_hat")
C_tilde = Operator(sandwich=linear_sandwich(util.C_tilde), name="C_tilde")
W = Operator(sandwich=linear_sandwich(util.W), name="W")

H = T + C_tilde + W  # Our Hamiltonian!

I = Operator(sandwich=linear_sandwich(util.N), name="I")

# Handy shortcut.
def inner_product(l, r, *args, **kwargs):
    return I.sandwich(l, r, *args, **kwargs)

T_validator = linear_sandwich(testutils.T_validator)
C_validator = linear_sandwich(testutils.C_validator)
W_validator = linear_sandwich(testutils.W_validator)
N_validator = linear_sandwich(testutils.N_validator)
I_validator = N_validator
