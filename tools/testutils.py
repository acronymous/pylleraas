#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 22:05:08 2021

@author: william

Utility functions specific to testing the software.
"""

import numpy as np

from . import util

#
# Validation functions for the N, T, C, and W operators on Hylleraas' basis.
# Dictionaries are index by (J, K, M).
#
N_dict = {(0, 0, 0): lambda L: 1,  
          (0, 0, 1): lambda L: (35/16)*(L**-1), (0, 0, 2): lambda L: (6)*(L**-2),
          (0, 2, 0): lambda L: (3/2)*(L**-2),  (0, 2, 1): lambda L: (77/16)*(L**-3), (0, 4, 0): lambda L: (9)*(L**-4)}


def N_validator(j1, j2, k1, k2, m1, m2, L):
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    return (np.pi**2)*(L**-6)*N_dict[(J, K, M)](L)


C_dict = {(0, 0, 0): lambda L: 1,
          (0, 0, 1): lambda L: (30/16)*(L**-1), (0, 0, 2): lambda L: (9/2)*(L**-2),
          (0, 2, 0): lambda L: (3/2)*(L**-2),  (0, 2, 1): lambda L: (70/16)*(L**-3), (0, 4, 0): lambda L: (9)*(L**-4)}


def C_validator(j1, j2, k1, k2, m1, m2, L):
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    Z = 2
    #return -2*Z*(Q_E**2)*(np.pi**2)*(L**-5)*C_dict[(J, K, M)](L)
    return 2*(np.pi**2)*(L**-5)*C_dict[(J, K, M)](L)


W_dict = {(0, 0, 0): lambda L: 1,  
          (0, 0, 1): lambda L: 1.6*(L**-1),  (0, 0, 2): lambda L: 3.5*(L**-2), 
          (0, 2, 0): lambda L: 0.9*(L**-2), (0, 2, 1): lambda L: 2.4*(L**-3), (0, 4, 0): lambda L: 3.9*(L**-4)}


def W_validator(j1, j2, k1, k2, m1, m2, L):
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    return (5/8)*(util.Q_E**2)*(np.pi**2)*(L**-5)*W_dict[(J, K, M)](L)


T_dict = {(0, 0, 0): lambda L: 1,  
          (0, 0, 1): lambda L: (25/16)*(L**-1), (0, 0, 2): lambda L: (4)*(L**-2),     
          (0, 2, 0): lambda L: (3/2)*(L**-2),   (0, 2, 1): lambda L: (73/16)*(L**-3), (0, 4, 0): lambda L: (15)*(L**-4)}


def T_validator(j1, j2, k1, k2, m1, m2, L):
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    return (util.H_BAR**2)*(util.M_E**-1)*(np.pi**2)*(L**-4)*T_dict[(J, K, M)](L)
