#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 12 16:25:46 2021

@author: william

General utility module for physical constants, algebraic functions, and other
useful functions.
"""

import fractions
import functools

import numpy as np
import scipy.constants

#
# Re-exporting physical constants under shorter names.
# Going to use Hartree units as they *massively* simplify our calculations,
# and will keep the multiple-order-of-magnitude swings in floats down.
# 

# These are all unity in Hartree units!
Q_E = 1
M_E = 1
H_BAR = 1
R_0 = 1

H = 2*np.pi  # from the reduced Planck constant definition 

#
# Some physical constants in SI units
#
Q_E_SI = scipy.constants.elementary_charge
M_E_SI = scipy.constants.electron_mass
H_BAR_SI = scipy.constants.hbar
H_SI = scipy.constants.h

# Hartree energy in joules and electron-volts.
E_H = 2*scipy.constants.value("Rydberg constant times hc in J")
E_H_EV = 2*scipy.constants.value("Rydberg constant times hc in eV")


#
# Useful utility function
#
energy_constant = (Q_E**2)/R_0

def analytical_energy_000(k):
    return energy_constant*(4/k**2 - 27/(4*k))

#
# L factor: Free parameter for minimising eigenvalues.
# a function for converting back and forth between this and the kappa value
# given in the literature.
#
def L_k_swap(k_or_L, Z=2):
    return Z/(k_or_L*R_0)

def Nr(j1, j2, k1, k2, m1, m2):
    "Inner product operator: rational part"
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    if any((m := n, i == 0)[1] for n, i in enumerate([M+2, K+1, K+3, K+M+3, K+M+5])): 
        # Forbidding explicitly non-normalisable states.
        return 0
    if K % 2 != 0:
        return 0
    else:
        numerator = 2 * np.math.factorial(J + K + M + 5)
        denominator = (M + 2)*(2**(J + K + M + 6))
        fraction_sum = 1/(K+1) - 1/(K+3) - 1/(K+M+3) + 1/(K+M+5)
        return (numerator/denominator)*fraction_sum
    
def N(j1, j2, k1, k2, m1, m2, L):
    "Inner product operator"
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    return (np.pi**2)*(L**-(J + K + M + 6))*Nr(j1, j2, k1, k2, m1, m2)


def C_hat(j1, j2, k1, k2, m1, m2, L, Z=2):
    "Main nuclear coulomb interaction operator"
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    if any((m := n, i == 0)[1] for n, i in enumerate([M+2, K+1, K+M+3])): 
        # Preventing division by zero: these states presumably don't normalise.
        # raise Exception(f"{m}")
        return 0
    if K % 2 != 0:
        return 0
    else:
        numerator = 8 * (np.pi**2) * np.math.factorial(J + K + M + 4)
        denominator = (M + 2)*((2*L)**(J + K + M + 5))
        fraction_sum = 1/(K+1) - 1/(K+M+3)
        return (numerator/denominator)*fraction_sum


def C_tilde(j1, j2, k1, k2, m1, m2, L, Z=2):
    "Nuclear coulomb interaction operator - includes charge coefficients"
    return -Z*(Q_E**2)*C_hat(j1, j2, k1, k2, m1, m2, L, Z=2)


def W(j1, j2, k1, k2, m1, m2, L, q=Q_E):
    "Inter-electron EM interaction operator"
    return (q**2)*N(j1, j2, k1, k2, m1-1, m2, L)


def T(j1, j2, k1, k2, m1, m2, L, m=M_E):
    "Kinetic energy operator"
    J = j1 + j2
    M = m1 + m2

    # Computing N and C coefficients. Underscores for added values
    N000  = N(j1, j2, k1, k2, m1, m2, L) 
    N100  = N(j1-1, j2, k1, k2, m1, m2, L)
    
    N200  = N(j1-2, j2, k1, k2, m1, m2, L)
    N020  = N(j1, j2, k1-2, k2, m1, m2, L)
    N002  = N(j1, j2, k1, k2, m1-2, m2, L)
    
    C100  = C_hat(j1-1, j2, k1, k2, m1, m2, L)
    C1_22 = C_hat(j1-1, j2, k1+2, k2, m1-2, m2, L)
    
    C_102 = C_hat(j1+1, j2, k1, k2, m1-2, m2, L)
    
    C000  = C_hat(j1, j2, k1, k2, m1, m2, L)
    C0_22 = C_hat(j1, j2, k1+2, k2, m1-2, m2, L)
    
    double_term = ((L**2)*N000 - J*L*N100 
                   + j1*j2*N200 + k1*k2*N020 + m1*m2*N002)
    
    half_term = (  (m1*j2 + m2*j1)*(C100 - C1_22)
                 + (m1*k2 + m2*k1)*(C_102 - C100)
                 - M*L*(C000 - C0_22))
    
    return (H_BAR**2)/(2*m) * (2*double_term + half_term/2)
