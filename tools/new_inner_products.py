#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 22:02:21 2021

@author: william
"""

import fractions
import functools

import numpy as np

import util

#
# Various inner product functions for algebra.HyllerasState
# To be replaced with fraction-float functions?
#
    
@functools.lru_cache()
def new_N_rational(J, K, M):
    """
    Inner product operator: rational part. Takes J, K, and M and returns an
    instance of fractions.Fraction, or integer zero.
    """
    if any(i == 0 for i in [M+2, K+1, K+3, K+M+3, K+M+5]): 
        # Forbidding explicitly non-normalisable states.
        return 0
    if K % 2 != 0:
        return 0 # By definition
    else:
        numerator = 2 * np.math.factorial(J + K + M + 5)
        denominator = (M + 2)*(2**(J + K + M + 6))
        fraction_sum = (fractions.Fraction(1, K+1) 
                        - fractions.Fraction(1, K+3)
                        - fractions.Fraction(1, K+M+3)
                        + fractions.Fraction(1, K+M+5))
        return fractions.Fraction(numerator, denominator)*fraction_sum

def new_N(j1, j2, k1, k2, m1, m2, L):
    "Inner product operator"
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    return (np.pi**2) * (L**-(J + K + M + 6)) * new_N_rational(J, K, M)

@functools.lru_cache()
def C_hat_rational(J, K, M):
    if any(i == 0 for i in [M+2, K+1, K+M+3]): 
        # Preventing explicitly non-normalisable states.
        return 0
    if K % 2 != 0:
        return 0
    else:
        numerator = 8 * np.math.factorial(J + K + M + 4)
        denominator = M + 2
        fraction_sum = (fractions.Fraction(1, K+1) 
                        - fractions.Fraction(1, K+M+3))
        return fractions.Fraction(numerator, denominator)*fraction_sum

def new_C_hat(j1, j2, k1, k2, m1, m2, L):
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    return (np.pi**2) * ((2*L)**-(J + K + M + 5)) * C_hat_rational(J, K, M)

@functools.lru_cache()
def C_tilde_rational(J, K, M, Z=2):
    return Z*C_hat_rational(J, K, M)

def new_C_tilde(j1, j2, k1, k2, m1, m2, L):
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    return (util.Q_E**2)*C_tilde_rational(J, K, M)

def new_W(j1, j2, k1, k2, m1, m2, L, q=util.Q_E):
    return (q**2) * new_N(j1, j2, k1, k2, m1-1, m2, L)

@functools.lru_cache()
def new_T_rational(j1, j2, k1, k2, m1, m2):
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2

    # Computing rational N and C coefficients. Underscores for added values
    N000  = new_N_rational(J, K, M)
    N100  = new_N_rational(J-1, K, M)
    
    N200  = new_N_rational(J-2, K, M)
    N020  = new_N_rational(J, K-2, M)
    N002  = new_N_rational(J, K, M-2)
    
    C100  = C_hat_rational(J-1, K, M)
    C1_22 = C_hat_rational(J-1, K+2, M-2)
    
    C_102 = C_hat_rational(J+1, K, M-2)
    
    C000  = C_hat_rational(J, K, M)
    C0_22 = C_hat_rational(J, K+2, M-2)
    
    double_term = (N000 - J*N100 + j1*j2*N200 + k1*k2*N020 + m1*m2*N002)
    
    inv_power_of_2 = fractions.Fraction(1, 2**(J+K+M+4))
    
    half_term = inv_power_of_2*(M*(C0_22 - C000)/2 
                 + (m1*j2 + m2*j1)*(C100 - C1_22)
                 + (m1*k2 + m2*k1)*(C_102 - C100))
    
    return 2*double_term + half_term/2

def new_T(j1, j2, k1, k2, m1, m2, L, m=util.M_E):
    J = j1 + j2
    K = k1 + k2
    M = m1 + m2
    return (util.H_BAR**2)/(2*m) * np.pi**2 * L**-(J+K+M+4) * new_T_rational(j1, j2, k1, k2, m1, m2)